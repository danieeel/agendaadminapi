module.exports = {
    buildTo: function (status_code, msg, success, res) {
        var to = new Object(); 
        to.status_code = status_code;
        to.message = msg;
        to.success = success;
        return to;
    },
    
    buildUserFromResponse: function (doc, id) {
        var user = new Object();
        user.id = id;
        user.name = doc.name;
        user.email = doc.email;
        return user;
    },

    buildUserFromRequestBody: function (body) {
        var user = new Object();
        user.id = body.user.id;
        user.name = body.user.name;
        user.email = body.user.email;
        return user;
    },

    
}