const Google = require('googleapis');
const BUCKET = 'agenda-admin';

module.exports = {
    authenticateAndExecute: function (req, res, callback) {
        
        var accessToken = getAccessToken(req.get('Authorization'));
        var oauth = new Google.auth.OAuth2();
        oauth.setCredentials({ access_token: accessToken });

        var permission = 'storage.buckets.get';
        var gcs = Google.storage('v1');
        gcs.buckets.testIamPermissions(
            { bucket: BUCKET, permissions: [permission], auth: oauth }, {},
            function (err, response) {
                if (response && response['permissions'] && response['permissions'].includes(permission)) {
                    continueAuthorizedFlow(req, res, callback);
                } else {
                    console.log('forbidden: ' + err);
                    res.status(403).send("The request is forbidden.");
                }
            });
    }
};

function getAccessToken(header) {
    if (header) {
        var match = header.match(/^Bearer\s+([^\s]+)$/);
        if (match) {
            return match[1];
        }
    }

    return null;
}

function continueAuthorizedFlow(req, res, callback) {
    callback(req, res);
}