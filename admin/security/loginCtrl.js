var authCtrl = require('./../security/authenticatorCtrl.js');
const admin = require('firebase-admin');
const db = admin.firestore();

module.exports = {
    login: function (req, res) {
        login(req, res);
    }
};

function login(req, res) {
    console.log('logging: ', req.body.email, req.body.password);
    var email = req.body.email;
    var password = req.body.password;

    var userCollection = db.collection('admin_login');
    var login = userCollection
        .where("email", "==", email)
        .where("password", "==", password)
        .get()
        .then(ref => {
            var login = new Object();
            ref.forEach(doc => {
                console.log(JSON.stringify(doc));
                login.success = true;
                login.message = "Logged with success";
                login.token = "token here";
                console.log(JSON.stringify(login));
                res.status(200).send(JSON.stringify(login));
            });
            login.success = false;
            login.message = "Not logged";
            login.token = "";
            console.log(JSON.stringify(login));
            res.body = login;
            res.message = "access forbiden";
            res.status(401).send(JSON.stringify(login));
        })
        .catch(err => {
            console.log('Error on login', err);
            login.success = false;
            login.message = "Not logged";
            login.token = "";
            console.log(JSON.stringify(login));
            res.status(500).send(JSON.stringify(login));
        });
}
