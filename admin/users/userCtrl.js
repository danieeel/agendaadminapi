const admin = require('firebase-admin');
const functions = require('firebase-functions');
var baseResponseTo = require('../model/to/baseResponseTo.js');
var authCtrl = require('./../security/authenticatorCtrl.js');
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

module.exports = {
    getUsers: function (req, res) {
        // authCtrl.authenticateAndExecute(req, res, getUsers);
        getUsers(req, res);
    }
    // },

    // getUserById: function (req, res) {
    //     // authCtrl.authenticateAndExecute(req, res, getUsers);
    //     getUserById(req, res);
    // },

    // addUser: function (req, res) {
    //     // authCtrl.authenticateAndExecute(req, res, addUser);
    //     addUser(req, res);
    // },

    // editUser: function (req, res) {
    //     // authCtrl.authenticateAndExecute(req, res, editUser);
    //     editUser(req, res);
    // },

    // deleteUser: function (req, res) {
    //     // authCtrl.authenticateAndExecute(req, res, editUser);
    //     deleteUser(req, res);
    // }
};

function getUsers(req, res) {
    console.log('getting users');
    var user;
    var userList = [];
    var userCollection = db.collection('users');
    var allUsers = userCollection
        .get()
        .then(users => {
            users.forEach(doc => {
                var user = baseResponseTo.buildUserFromResponse(doc.data(), doc.id);
                userList.push(user);
            });
            // var to = baseResponseTo.buildTo(200, "List of users", true, res);
            // to.users = userList;
            res.send(JSON.stringify(userList));
        })
        .catch(err => {
            console.log('Error getting users', err);
            var to = baseResponseTo.buildTo(500, 'Error getting users: ' + err, false, res);
            res.send(to);
        });
}

// function getUserById(req, res) {
//     console.log('getting user by id: ', req.body.user.id);
//     var userId = req.body.user.id;
//     var userCollection = db.collection('users');
//     var user = userCollection
//         .doc(userId)
//         .get()
//         .then(doc => {
//             if (doc.exists) {
//                 var user = baseResponseTo.buildUserFromResponse(doc.data(), doc.id);
//                 var to = baseResponseTo.buildTo(200, "User exists", true, res);
//                 to.user = user;
//                 console.log(JSON.stringify(to));
//                 res.send(JSON.stringify(to));
//             } else {
//                 var to = baseResponseTo.buildTo(500, "User not found", false, res);
//                 res.status(500).send(JSON.stringify(to));
//             }
            
//         })
//         .catch(err => {
//             console.log('Error getting user by id', err);
//             var to = baseResponseTo.buildTo(500, 'Error getting user by id: ' + err, false, res);
//             res.send(to);
//         });
// }

// function addUser(req, res) {
//     res.set('Access-Control-Allow-Origin', '*');
//     console.log('adding new user');

//     var user = baseResponseTo.buildUserFromRequestBody(req.body);
//     var addDoc = db.collection('users').add({
//         name: user.name,
//         email: user.email
//     }).then(doc => {
//         console.log('Added user with Id: ', doc.id);
//         var to = baseResponseTo.buildTo(200, "New user added with success", true, res);
//         to.user = baseResponseTo.buildUserFromResponse(user, doc.id);
//         res.send(JSON.stringify(to));
//     })
//         .catch(err => {
//             console.log('Error adding user', err);
//             var to = baseResponseTo.buildTo(500, err, false, res);
//             res.send(to);
//         });
// }

// function editUser(req, res) {
//     res.set('Access-Control-Allow-Origin', '*');
//     console.log('editing user');

//     var user = baseResponseTo.buildUserFromRequestBody(req.body);
//     var addDoc = db.collection('users').doc(user.id).set({
//         name: user.name,
//         email: user.email
//     }).then(doc => {
//         console.log('Edited user with Id: ', doc.id);
//         var to = baseResponseTo.buildTo(200, "User edited with success", true, res);
//         to.user = baseResponseTo.buildUserFromResponse(user, doc.id);
//         res.send(JSON.stringify(to));
//     })
//         .catch(err => {
//             console.log('Error editing user', err);
//             var to = baseResponseTo.buildTo(500, err, false, res);
//             res.send(to);
//         });
// }

// function deleteUser(req, res) {
//     res.set('Access-Control-Allow-Origin', '*');
//     console.log('deleting user');

//     var user = baseResponseTo.buildUserFromRequestBody(req.body);
//     var addDoc = db.collection('users').doc(user.id)
//         .delete()
//         .then(doc => {
//             console.log('Deleted user');
//             var to = baseResponseTo.buildTo(200, "User deleted with success", true, res);
//             res.send(JSON.stringify(to));
//         })
//         .catch(err => {
//             console.log('Error deleting user', err);
//             var to = baseResponseTo.buildTo(500, err, false, res);
//             res.send(to);
//         });
// }
