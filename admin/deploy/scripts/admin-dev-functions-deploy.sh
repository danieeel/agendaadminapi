#!/bin/bash
clear
echo "#####################################################################################################"
echo "WELCOME TO AGENDA ADMIN MICROSERVICES DEPLOYER"
echo "#####################################################################################################\n\n"

read -r -p "Start deploy? " input

    case $input in [yY][eE][sS]|[yY])
        echo "\nDeploying getUser... \n"
        gcloud beta functions deploy getUsers  --trigger-http
        echo "\nDeploying getUserById... \n"
        gcloud beta functions deploy getUserById  --trigger-http
        echo "\nDeploying addUser... \n"
        gcloud beta functions deploy addUser  --trigger-http
        echo "\nDeploying editUser... \n"
        gcloud beta functions deploy editUser  --trigger-http
        echo "\nDeploying deleteUser... \n"
        gcloud beta functions deploy deleteUser  --trigger-http
        echo "\nDeploying login... \n"
        gcloud beta functions deploy login  --trigger-http
        echo "\n\n#####################################################################################################"
        echo "ALL MICROSERVICES DEPLOYED"
        echo "#####################################################################################################"
    ;;
 
    [nN][oO]|[nN])
        echo "\n\n#####################################################################################################"
        echo "DEPLOY CANCELLED"
        echo "#####################################################################################################"
    ;;
    
    *)
    esac