var userCtrl = require('./users/userCtrl.js');
var loginCtrl = require('./security/loginCtrl.js');

exports.getUsers = (req, res) => {
    userCtrl.getUsers(req, res);
}

// exports.getUserById = (req, res) => {
//     userCtrl.getUserById(req, res);
// }

// exports.addUser = (req, res) => {
//     userCtrl.addUser(req, res);
// }

// exports.editUser = (req, res) => {
//     userCtrl.editUser(req, res);
// }

// exports.deleteUser = (req, res) => {
//     userCtrl.deleteUser(req, res);
// }

exports.login = (req, res) => {
    loginCtrl.login(req, res);
}
